﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal class RandomNumberStrategy : IGameStrategy
    {
        public int GenerateNumber(IGameConfig config)
        {
            Random random = new Random();
            return random.Next(config.MinNumber, config.MaxNumber + 1);
        }
    }
}
