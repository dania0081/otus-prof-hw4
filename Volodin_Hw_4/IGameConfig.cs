﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal interface IGameConfig
    {
        int MaxGuesses { get; set; }
        int MinNumber { get; set; }
        int MaxNumber { get; set; }
    }
}
