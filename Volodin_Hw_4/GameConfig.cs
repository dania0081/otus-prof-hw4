﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal class GameConfig : IGameConfig
    {
        public int MaxGuesses { get; set; }
        public int MinNumber { get; set; }
        public int MaxNumber { get; set; }
    }
}
