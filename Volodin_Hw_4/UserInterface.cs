﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal class UserInterface : IUserInterface
    {
        public int GetUserGuess()
        {
            Console.Write("Guess a number: ");
            return int.Parse(Console.ReadLine());
        }

        public void PrintMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
