﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal interface IGameStrategy
    {
        int GenerateNumber(IGameConfig config);
    }
}
