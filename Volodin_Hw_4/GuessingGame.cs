﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volodin_Hw_4
{
    internal class GuessingGame
    {
        private IUserInterface _ui;
        private IGameStrategy _strategy;
        private IGameConfig _gameConfig;
        public int _numberToGuess;
        
        public GuessingGame(IUserInterface ui, IGameStrategy strategy, IGameConfig gameConfig)
        {
            _ui = ui;
            _strategy = strategy;
            _gameConfig = gameConfig;

            _numberToGuess = _strategy.GenerateNumber(_gameConfig);
        }

        public bool Guess(int guess)
        {
            if (guess == _numberToGuess)
            {
                return true;
            }
            else if (guess < _numberToGuess)
            {
                _ui.PrintMessage("Too low!");
            }
            else
            {
                _ui.PrintMessage("Too high!");
            }
            return false;
        }
    }
}
