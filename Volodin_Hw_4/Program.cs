﻿namespace Volodin_Hw_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var gameConfig = new GameConfig
            {
                MaxGuesses = 10, // максимальное количество попыток
                MinNumber = 1, // минимальное число для угадывания
                MaxNumber = 100 // максимальное число для угадывания
            };

            var ui = new UserInterface();

            // получаем стратегию для игры
            // здесь используем RandomNumberStrategy для генерации числа
            var strategy = new RandomNumberStrategy();

            var game = new GuessingGame(ui, strategy, gameConfig);

            bool guessed = false;
            int guessesLeft = gameConfig.MaxGuesses;

            ui.PrintMessage($"Guess a number between {gameConfig.MinNumber} and {gameConfig.MaxNumber}");

            while (!guessed && guessesLeft > 0)
            {
                int guess = ui.GetUserGuess();

                guessed = game.Guess(guess);

                guessesLeft--;

                if (!guessed && guessesLeft > 0)
                {
                    ui.PrintMessage($"You have {guessesLeft} guesses left.");
                }
            }

            if (guessed)
            {
                ui.PrintMessage("Congratulations, you guessed the number!");
            }
            else
            {
                ui.PrintMessage($"Sorry, the number was {game._numberToGuess}.");
            }
        }
    }
}